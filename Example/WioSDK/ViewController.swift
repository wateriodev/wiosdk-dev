import UIKit
import WioSDK
import WaterIOBleKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource:  [String] = []
    private let email = "israel50@water-io.com"
    private let password = "Israel770"
    
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        WIOApp.shared.processor.delegate = self
        //login()
    }
    
    @IBAction func onClickButton(_ sender: Any) {
        let vc = CapConnectionViewController()
        let nvc = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nvc, animated: true, completion: nil)
    }
    
    @IBAction func onClickReminder(_ sender: Any) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellId") else { fatalError() }
        cell.textLabel?.text = dataSource[indexPath.row]
        return cell
    }
    
    func register() {
        WIOApp.shared.wioNetwork.wioApi.register(email: email,
                                                 password: password,
                                                 isConfirmTerms: true,
                                                 isConfirmAdvertising: false) { result in
                                                    switch result {
                                                    case .success(let response):
                                                        print("success:" + "\(response)")
                                                        break
                                                    case .error(let error):
                                                        print("error:" + "\(error)")
                                                        
                                                        break
                                                    }
        }
    }
    
    func login() {
        WIOApp.shared.wioNetwork.wioApi.login(email: email, password: password) { result in
            switch result {
            case .success(let response):
                print("response:" + "\(response)")
                WIOApp.shared.wioNetwork.wioAnalytics.tracker.login()
            case .error(let error):
                print("error:" + "\(error)")
                break
            }
        }
    }
    
}

extension ViewController: WIOCapDataProcessorDelegate {
    
    func capDataModelDidUpdateEvents(_ capDataModel: CapDataModel, eventsList: CapEventsList, isCapOpen: Bool?) {
        for events in eventsList.events {
            insertEvent(name: "\(events.timestamp) | \(events.code) | \(events.opCode)")
        }
    }
    
    func didUpdateCapInfo(_ capVersion: String) {
        insertEvent(name: "capVersion \(capVersion)")
    }
    
    func insertEvent(name: String) {
        DispatchQueue.main.async {
            self.dataSource.insert("\(name) | \(self.dataSource.count)", at: 0)
            self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }
    }
}
