//
//  AppDelegate.swift
//  WioSDK
//
//  Created by israel@water-io.com on 07/29/2019.
//  Copyright (c) 2019 israel@water-io.com. All rights reserved.
//

import UIKit
import WioSDK
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        WIOApp.shared.start(appToken: Configuration.appToken, appType: Configuration.appType)
//        let chat = WIOChatManager(waterIOClient: WIOApp.shared.wioNetwork.wioApi)
//
//
//        if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
//            as! [NSObject : AnyObject]? {
//            print("remoteNotification \(remoteNotification)")
//        }
        
        
        //Setup notifcations
        //UNUserNotificationCenter.current().delegate = self
        
        
        //Setup remote notifications
        let configuration = PushConfiguration(appID: Configuration.pushAppID,
                                              appType: Configuration.appType,
                                              apiKey: Configuration.pushApiKey)
        WIOApp.shared.wioNotificationService.start(configuration: configuration, launchOptions: launchOptions)
        UIApplication.shared.registerForRemoteNotifications()

        
        //sendLocalNotification()
        return true
    }
    
    func sendLocalNotification() {
        let scheduler = WIOLocalNotification()
        let triggerDate = Date().addingTimeInterval(TimeInterval(5.0))
        let firstNotification = WIONotification(identifier: "123",
                                                alertTitle: "Notification Alert",
                                                alertBody: "You have successfully created a notification",
                                                date: triggerDate, repeats: .none,
                                                userInfo: ["aps": ["category" :"123"]])
        scheduler.scheduleNotification(notification: firstNotification)
    }
}

extension AppDelegate {
    
    //For local notificaiton when click action
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        WIOApp.shared.wioNotificationService.onUserClickAction(center, didReceive: response)
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        WIOApp.shared.wioNotificationService.onPushReceived(userInfo: userInfo, onStart: false)
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        WIOApp.shared.wioNotificationService.onDidRegisterForRemoteNotifications(withDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        WIOApp.shared.wioNotificationService.onDidFailToRegisterForRemoteNotificationsWithError(error)
    }
    
    //Optional when you want the application will show alert when app is active
    //using UNUserNotificationCenter.current().delegate = self
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        WIOApp.shared.wioNotificationService.onPushReceived(userInfo: notification.request.content.userInfo,
                                                            onStart: false)
        completionHandler(.alert)
    }
}
