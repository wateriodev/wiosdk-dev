import Foundation

public final class CapCommandSender {
    private var bleManager = WIOApp.shared.wioBleManager
    public init(){}
    
    public func setReminder(hour: Int, minutes: Int) {
        print("set Reminder Command  waiting until cap communicates with a device")
        bleManager.setVitaminsReminder(hours: hour, minutes: minutes)
    }
    
    public func setReminders(reminders: [(hours: Int, minutes: Int)]) {
        print("set setReminders Command  waiting until cap communicates with a device")
        bleManager.setVitaminsReminders(reminders: reminders)
    }
    
    public func runBlinking() {
        print("set runBlinking Command  waiting until cap communicates with a device")
        bleManager.sendCommand(command: .runBlinking, isStore: false)
    }
    
    public func playSound() {
        print("set playSound Command  waiting until cap communicates with a device")
        bleManager.sendCommand(command: .playSound(tune: 1), isStore: false)
    }
}
