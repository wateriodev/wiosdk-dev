//
//  EventCode.swift
//  WioSDK
//
//  Created by israel on 21/11/2019.
//

import Foundation

public enum EventCode: UInt8, RawRepresentable {
    case pair               = 0x50
    case vitaminsReminder   = 0x81
    case open               = 0x4f
    case close              = 0x43
    case appConnected       = 0x41
    case appDisconnected    = 0x44
    case unknown            = 0x00
}
