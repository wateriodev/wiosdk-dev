import Foundation
import WaterIONetwork
import WaterIOBleKit
import CoreBluetooth
//TODO: change class to name to HardwareConnection like android
public class CapConnection: CapConnectionType {
    
    //Public
    public var delegate: CapConnectionDelegate?
    public var capUUID: String = ""
    public var state: ConnectionState {
        didSet {
            self.delegate?.onDeviceConnectionStateChange(state: state)
        }
    }
    public var statusText: String = ""
    
    //Private
    private var bleManager = WIOApp.shared.wioBleManager
    private var isStartingToScan: Bool = false
    
    public var isKeepConnection: Bool = false {
        didSet {
            bleManager.iskeepDeviceInConnection = isKeepConnection
        }
    }
    
    private var isDeviceConnected: Bool {
        return bleManager.isDeviceConnected
    }
    
    private var isDeviceRegisterd: Bool {
        return CurrentDevice.currentDevice() != nil
    }
    
    public init() {
        state = .unknown
        updateState()
        bleManager.scanDelegate = self
        setupNotification()
    }
    
    deinit {
        if bleManager.isScanning {
            bleManager.stopScanning()
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    public func startScan() {
        if isDeviceRegisterd {
            forgetDevice()
        }
        scanForDevices()
    }
    
    public func stopScan() {
        bleManager.stopScanning()
    }
    
    public func forgetDevice() {
        bleManager.onDidDisconnectedFromDevice = { [weak self] in self?.onDidDisconnectedToDevice() }
        bleManager.disconnect(forgetDevice: true)
        updateState()
    }
}

extension CapConnection: BleManagerScanDelegate {
    
    public func bleManager(_ bleManager: BleManagerType, didUpdateConnection isConnect: Bool) {
        delegate?.onBleConnectionDidChange(isConnected: isConnect)
        updateState()
    }
    
    public func bleManager(_ bleManager: BleManagerType, centralStateDidChange state: CBManagerState) {
        if state != .poweredOn {
            if case .scanning = self.state {
                bleManager.stopScanning()
            }
        } else if isStartingToScan {
            scanForDevices()
        }
        updateState()
    }
    
    public func bleManager(_ bleManager: BleManagerType, didUpdate availableDevices: [String] ) {
    }
}


private extension CapConnection {
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive(_:)),
                                               name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func applicationDidBecomeActive(_ notification: NSNotification) {
        if CurrentDevice.currentDevice() == nil {
            scanForDevices()
        }
    }
}

private extension CapConnection {
    func updateState() {
        let oldState: ConnectionState = state
        let newState: ConnectionState
        let isScanning = bleManager.isScanning
        let managerState = bleManager.managerState
        
        if let device = CurrentDevice.currentDevice() {
            capUUID = device.UUID
            if case .scanning = oldState {
                newState = .connected(newDevice: true)
            } else {
                newState = .connected(newDevice: false)
            }
            statusText = "Connected."
        } else if (isStartingToScan || isScanning) && managerState == .poweredOn {
            capUUID = ""
            newState = .scanning
            statusText = "Open or Close the cap to connect"
        } else if managerState != .poweredOn {
            switch managerState {
            case .resetting:
                statusText = "Bluetooth is restting"
                newState = .bluetoothIssue
            case .unauthorized:
                statusText = "Bluetooth unautorized"
                newState = .unauthorized
            case .unsupported:
                statusText = "Bluetooth is unsupported"
                newState = .bluetoothIssue
            case .poweredOff:
                statusText = "Bluetooth is off. Use the control center to turn it on"
                newState = .bluetoothIssue
            default:
                statusText = "Unknown issue"
                newState = .bluetoothIssue
            }
        } else {
            capUUID = ""
            newState = .unknown
            statusText = ""
        }
        
        self.state = newState
    }
    
    func scanForDevices() {
        if bleManager.managerState != .poweredOn {
            updateState()
            return
        }
        
        bleManager.onDidConnectToDevice = { [weak self] in self?.onDidConnectToDevice() }
        bleManager.scanForPeripherals()
        isStartingToScan = true
        updateState()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) { [weak self] in
            self?.updateState()
        }
        //Analytics.tracker.scanningCap()
    }
    
    func onDidDisconnectedToDevice() {
        updateState()
    }
    
    func onDidConnectToDevice() {
        isStartingToScan = false
        updateState()
    }
}
