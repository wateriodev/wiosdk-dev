import Foundation
import WaterIOBleKit

public struct CapEvent {
    public let timestamp: UInt32
    public let value: UInt16
    public var opCode: EventCode
    
    init(entry: CapEventEntry) {
        self.timestamp = entry.timestamp
        self.opCode = EventCode(rawValue: entry.code) ?? .unknown
        self.value = entry.value
    }
}
