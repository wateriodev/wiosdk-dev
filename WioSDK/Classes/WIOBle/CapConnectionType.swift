import Foundation

public enum ConnectionState {
    case unknown
    case connected(newDevice: Bool)
    case scanning
    case bluetoothIssue
    case unauthorized
}

public protocol CapConnectionType {
    var capUUID: String { get }
    var statusText: String { get }
    var state: ConnectionState { get }
    var delegate: CapConnectionDelegate? { get }
    var isKeepConnection: Bool { get set }

    func startScan()
    func stopScan()
    func forgetDevice()
}

public protocol CapConnectionDelegate: class {
    func onDeviceConnectionStateChange(state: ConnectionState)
    func onBleConnectionDidChange(isConnected: Bool)
}

