import Foundation
import WaterIOBleKit
import WaterIONetwork
import CocoaLumberjack

public protocol WIOCapDataProcessorDelegate: class {
    func capDataModelDidUpdateEvents(_ capDataModel: CapDataModel, eventsList: CapEventsList, isCapOpen: Bool?)

    func didUpdateCapInfo(_ capVersion: String)
}

public class WIOCapDataProcessor {

    public weak var delegate: WIOCapDataProcessorDelegate?
    
    public init() {
        DDLogInfo("CapDataProcessor created")
    }
}

extension WIOCapDataProcessor: CapDataModelDelegate {
    
    public func didUpdateCapInfo(_ capInfo: CapInfo) {
        delegate?.didUpdateCapInfo(capInfo.capVersion)
    }
    
    public func capDataModelDidUpdateEvents(_ capDataModel: CapDataModel, eventsList: CapEventsList, isCapOpen: Bool?) {
        delegate?.capDataModelDidUpdateEvents(capDataModel, eventsList: eventsList, isCapOpen: isCapOpen)
    }
}
