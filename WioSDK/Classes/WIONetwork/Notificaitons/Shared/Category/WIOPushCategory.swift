//
//  WIOPushCategory.swift
//  WioSDK
//
//  Created by israel on 10/12/2019.
//

import Foundation

public enum WIOPushCategory: String {
    case unknow = "unknow"
    case newChatMessage = "CHAT_NEW_MESSAGE"
    case pushWithButton = "PUSH_WITH_BUTTONS"
    
    //Buttons Id
    static let okButtonId = "push.action.ok"
    static let cancelButtonId = "push.action.cancel"
}

struct WIOPushConfiguration {
    //FIXME: now only 1 category support, category ID overide the second one
    
    func setupAllCategories() {
        setupYesNoCategory()
        //setupSendMessageCategory()
    }
    
    func setupYesNoCategory() {
        let scheduler = WIOLocalNotification()
        let okButtonId = WIOPushCategory.okButtonId
        let cancelButtonId =  WIOPushCategory.cancelButtonId
        let category = WIONotificationCategory(categoryIdentifier: WIOPushCategory.pushWithButton.rawValue)
        category.addActionButton(identifier: okButtonId, title: "Yes")
        category.addActionButton(identifier: cancelButtonId, title: "No")
        scheduler.scheduleCategories(categories: [category])
    }
    
    func setupSendMessageCategory() {
        let scheduler = WIOLocalNotification()
        let okButtonId = "send_Button"
        let cancelButtonId = "cancel_Button"
        let category = WIONotificationCategory(categoryIdentifier: WIOPushCategory.newChatMessage.rawValue)
        category.addActionButton(identifier: okButtonId, title: "Send")
        category.addActionButton(identifier: cancelButtonId, title: "Cancel")
        scheduler.scheduleCategories(categories: [category])
    }
}


