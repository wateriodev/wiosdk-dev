//READ more: https://github.com/d7laungani/DLLocalNotifications

public class WIOLocalNotification: NSObject {
    
    public func scheduleCategories(categories: [WIONotificationCategory]) {
        var notificationCategories = Set<UNNotificationCategory>()
        for category in categories {
            guard let categoryInstance = category.categoryInstance else { continue }
            notificationCategories.insert(categoryInstance)
        }
        UNUserNotificationCenter.current().setNotificationCategories(notificationCategories)
    }
    
    public func scheduleNotification(notification: WIONotification) {
        let content = UNMutableNotificationContent()
        content.title = notification.alertTitle ?? ""
        content.body = notification.alertBody ?? ""
        content.sound = .default
        
        if let userInfo = notification.userInfo {
            content.userInfo = userInfo
        }
        //content.badge = NSNumber(value: UIApplication.shared.applicationIconBadgeNumber + 1)
        content.categoryIdentifier = notification.identifier!

        let trigger = UNCalendarNotificationTrigger(dateMatching:
            convertToNotificationDateComponent(notification: notification,
                                               repeatInterval: notification.repeatInterval),
                                                    
                                                    repeats: notification.repeats)
        
        let request = UNNotificationRequest(identifier: notification.identifier!, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
            if error != nil {
                print("add NotificationRequest succeeded!")
            } else {
                print("add NotificationRequest error: \(String(describing: error))!")
            }
        }
    }
    
    public func cancelAlllNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    public func getPendingNotificationRequests(completionHandler: @escaping ([UNNotificationRequest]) -> Void) {
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: completionHandler)
    }
    
    public func getPendingNotification(_ id: String, completionHandler: @escaping (UNNotificationRequest?) -> Void) {
        getPendingNotificationRequests { (notifications) in
            let notification = notifications.filter{$0.identifier == id}.first
            completionHandler(notification)
        }
    }
    
    public func cancelNotification(notification: WIONotification) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [(notification.localNotificationRequest?.identifier)!])
        notification.scheduled = false
    }
    
    public func cancelNotification(_ id: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id])
    }
    
    private func convertToNotificationDateComponent(notification: WIONotification, repeatInterval: RepeatingInterval   ) -> DateComponents {
        
        var newComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second ], from: notification.fireDate!)
        
        if repeatInterval != .none {
            
            switch repeatInterval {
            case .minute:
                newComponents = Calendar.current.dateComponents([ .second], from: notification.fireDate!)
            case .hourly:
                newComponents = Calendar.current.dateComponents([ .minute], from: notification.fireDate!)
            case .daily:
                newComponents = Calendar.current.dateComponents([.hour, .minute], from: notification.fireDate!)
            case .weekly:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .weekday], from: notification.fireDate!)
            case .monthly:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .day], from: notification.fireDate!)
            case .yearly:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .day, .month], from: notification.fireDate!)
            default:
                break
            }
        }
        
        return newComponents
    }
}
