import Foundation
import FirebaseCore

public struct PushConfiguration {
    
    let appID: String
    let senerId: String
    let appType: String
    let apiKey: String

    public init(appID: String, appType: String, apiKey: String) {
        self.apiKey = apiKey
        self.appID = appID
        self.senerId = appID.components(separatedBy: ":")[1]
        self.appType = appType
    }
    
    func firebaseOption() -> FirebaseOptions {
        let option = FirebaseOptions(googleAppID: appID, gcmSenderID: senerId)
        option.apiKey = apiKey
        return option
    }
}
