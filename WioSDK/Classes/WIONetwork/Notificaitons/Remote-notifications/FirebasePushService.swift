import Foundation
import FirebaseMessaging
import FirebaseCore

class FirebasePushService: NSObject {
    
    private let gcmMessageIDKey = "gcm.message_id"
    private let muteNotificationKey = "user.disable.notification"
    private var firebaseConfiguration: PushConfiguration?
    
//    internal var userId: Int?  {
//        didSet {
//            if userId != nil {
//                subscribeToTopics()
//            }
//        }
//    }
    
    internal func startFirebase(option: PushConfiguration) {
        self.firebaseConfiguration = option
        FirebaseApp.configure(options: option.firebaseOption())
        Messaging.messaging().delegate = self
    }
    
    internal func updateUid() {
        subscribeToTopics()
    }
    
    internal func onDidRegisterForRemoteNotifications(token: Data) {
        Messaging.messaging().setAPNSToken(token, type: .prod)
        subscribeToTopics()
    }
    
    internal func onPushReceived(userInfo: [AnyHashable : Any]?, onStart: Bool) {
        if let userInfo = userInfo {
            print("Invalid User Info")
            Messaging.messaging().appDidReceiveMessage(userInfo)
        }
    }
    
    internal func subscribeToTopics() {
        guard let appType =  WIOApp.shared.appType,
            let uid = WIOApp.shared.userId else {
                print("failed to subscribe to topics")
                return
        }
        
        Messaging.messaging().subscribe(toTopic: "news")
        Messaging.messaging().subscribe(toTopic: "user-all")
        Messaging.messaging().subscribe(toTopic: "user-\(appType)")
        Messaging.messaging().subscribe(toTopic: "user-\(uid)") { error in
            if let error = error {
                print("failed to subscribe to topics \(error)")
            } else {
                print("registers to subscribe to topic \(uid)")
            }
        }
    }
    
    internal func unSubscribeFromTopics() {
        guard  let appType = WIOApp.shared.appType,
            let uid = WIOApp.shared.userId else {
                print("failed to unsubscribe to topics")
                return
        }
        Messaging.messaging().unsubscribe(fromTopic: "news")
        Messaging.messaging().unsubscribe(fromTopic: "user-all")
        Messaging.messaging().unsubscribe(fromTopic: "user-\(appType)")
        Messaging.messaging().unsubscribe(fromTopic: "user-\(uid)")
    }
}

extension FirebasePushService: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Firebase remoteMessage \(messaging)  \(remoteMessage)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")
    }
}
