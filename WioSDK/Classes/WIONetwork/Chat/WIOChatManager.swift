//
//  WIOChatManager.swift
//  WioSDK
//
//  Created by israel on 09/12/2019.
//

import Foundation
import WaterIONetwork

public protocol WIOChatManagerDelegate: class {
    func onRecivedMessage(massages: [WIOPushMassage])
}

public class WIOChatManager {
    
    public weak var delegate: WIOChatManagerDelegate?
    public var massages: [WIOPushMassage] = []
    
    private let waterIOClient: WIOApi
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public init(waterIOClient: WIOApi) {
        self.waterIOClient = waterIOClient
        registerNotificaiton()
    }
    
    public func updateMessage(with id: String, status: MassageState) {
        updateMessage(id: id, status: status)
    }
    
    public func sendMessage(_ text: String) {
        let currentTime = Int(Date().timeIntervalSince1970)
        let message = WIOPushMassage(timestamp: currentTime,
                                     text: text,
                                     status: .pending)
        massages.append(message)
        sendMessage(id: message.id, text: message.text)
    }
    
    public func getAllMessages() -> [WIOPushMassage] {
        return massages
    }
    
    public func onRecivedMessage(userInfo: [AnyHashable : Any]?) {
        guard let json = userInfo as? [String: Any],
            let messages = json["messages"] as? String,
            let map = try? WIOPushMassage.convertToDictionary(from: messages) else {
                fatalError("Invalid data")
        }
        
        guard let id = map.keys.first,
            let value = map.values.first as? [String : Any] else {
                fatalError("Parsing failed")
        }
        
        let message = WIOPushMassage(json: value, id: id)
        massages.append(message)
        self.delegate?.onRecivedMessage(massages: [message])
    }
    
    func update() {
        print("update")
    }
}

//MARK: API
extension WIOChatManager {
    
    //TODO: change @escaping To Result
    public func getAllMissingMessage(completion: @escaping ([WIOPushMassage]) -> Void) {
        waterIOClient.getAllMissingMessage { result in
            switch result {
            case .success(let result):
                print("getAllMissingMessage" + "\(result)")
                guard let convert = result as? [String: [String: [String: Any]]] else {
                    return
                }
                
                var messages: [WIOPushMassage] = []
                for (key, value) in convert {
                    print(key)
                    for (subKey, subValue) in value {
                        messages.append(WIOPushMassage(json: subValue, id: subKey))
                    }
                }
                completion(messages)
            case .error(let error):
                completion([])
                print("getAllMissingMessage error" + "\(error)")
                
            }
        }
    }
    
    func sendMessage(id: String, text: String) {
        waterIOClient.sendMessage(id: id, text: text) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let result):
                print("sendMessage" + "\(result)")
                me.update()
            case .error(let error):
                print("sendMessage error" + "\(error)")
            }
        }
    }
    
    func updateMessage(id: String, status: MassageState) {
        waterIOClient.updateMessage(id: id, status: status.rawValue) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let result):
                print("updateMessage" + "\(result)")
                me.update()
            case .error(let error):
                print("updateMessage error" + "\(error)")
                
            }
        }
    }
}

extension WIOChatManager {
    private func registerNotificaiton() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onRecivedMessageObserver),
            name: NSNotification.Name(rawValue: WIOConstance.onPushNotificaitonRecived),
            object: nil)
    }
    
    @objc private func onRecivedMessageObserver(notification: NSNotification) {
        if let objcet = notification.object as? [AnyHashable : Any] {
            onRecivedMessage(userInfo: objcet)
        } else {
            assert(true, "Message is empty")
        }
    }
}
