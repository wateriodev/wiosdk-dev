import Foundation

public class WIONetwork {
    public var wioApi: WIOApi
    public var wioAnalytics: WIOAnalytics

    public init(appType: String, appToken: String, deviceId: String? = nil) {
        wioApi = WIOApi(appType: appType, appToken: appToken)
        wioAnalytics = WIOAnalytics(appType: appType, appToken: appToken, deviceId: deviceId)
    }
    
    public func supportRequest(message: String) {
        wioAnalytics.tracker.supportMessage(message: message)
    }
}
