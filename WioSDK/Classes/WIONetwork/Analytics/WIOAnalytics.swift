import Foundation
import WaterIONetwork

public class WIOAnalytics {
    
    let appType: String
    let appToken: String
    
    public let tracker: FirebaseClient
    
    public init(appType: String, appToken: String, deviceId: String? = nil) {
        self.appType = appType
        self.appToken = appToken
        let userId = UserCredentialStore().userCredentials?.userId
        let deviceId = deviceId
        self.tracker = FirebaseClient(appType: appType, userId: userId, deviceId: deviceId)
    }
}
