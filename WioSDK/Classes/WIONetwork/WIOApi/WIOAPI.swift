import Foundation
import WaterIONetwork

public protocol WIOApiType {
    func login(email: String, password: String, completion: @escaping (WaterIONetwork.Result<[String : Any]>) -> Void)
    func register(email: String, password: String, isConfirmTerms: Bool, isConfirmAdvertising: Bool, completion: @escaping (WaterIONetwork.Result<[String : Any]>) -> Void)
    func resetPassword(email: String, appType: String, completion: @escaping (WaterIONetwork.Result<String>) -> Void)
    
    func updateProfile(json: WaterIONetwork.JSONDictionary, completion: @escaping (WaterIONetwork.Result<String>) -> Void)
    
    
    func sendMessage(id: String, text: String, completion: @escaping (Result<[String: Any]>) -> Void)
    func updateMessage(id: String, status: String, completion: @escaping (Result<[String: Any]>) -> Void)
    
    func buildHydrationGoal(completion: @escaping (WaterIONetwork.Result<Int>) -> Void)
    
    func updateLogin(email: String, password: String, completion: @escaping (Result<[String: Any]>) -> Void)
    
    func getAllMissingMessage(completion: @escaping (Result<[String: Any]>) -> Void)
    
    func getVitaminsList(completion: @escaping (Result<[[String: Any]]>) -> Void)
    
    func getVitaminsList(completion: @escaping (Result<[String: Any]>) -> Void)
}

protocol WIOApiDelegate: class {
    func userIdUpdated(_ userId: Int)
}

public class WIOApi: WIOApiType {
    
    let appType: String
    let appToken: String
    let deviceId: String?
    
    internal let waterIOClient: WaterIOClient
    internal weak var delegate: WIOApiDelegate?
    
    public init(appType: String, appToken: String, deviceId: String? = nil) {
        self.appType = appType
        self.appToken = appToken
        self.deviceId = deviceId
        let credentials = UserCredentialStore().userCredentials
        let userId: Int? = credentials?.userId
        self.waterIOClient = WaterIOClient(userId: userId,
                                           appType: appType,
                                           appToken: appToken)
        
    }
    
    func updteUserId(data: [String: Any]) {
        guard let credentials = UserCredentialStore().userCredentials else {
            return
        }
        delegate?.userIdUpdated(credentials.userId)
    }
    
    public func login(email: String, password: String, completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.login(email: email, password: password) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let data):
                me.updteUserId(data: data)
                completion(result)
            case .error(let error):
                print("error:" + "\(error)")
                completion(result)
            }
        }
    }
    
    public func register(email: String, password: String, isConfirmTerms: Bool, isConfirmAdvertising: Bool, completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.register(email: email, password: password, isConfirmTerms: isConfirmTerms, isConfirmAdvertising: false) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let data):
                me.updteUserId(data: data)
                completion(result)
            case .error(let error):
                print("error:" + "\(error)")
                completion(result)
            }
        }
    }
    
    public func buildHydrationGoal(completion: @escaping (Result<Int>) -> Void) {
        waterIOClient.requestBuildHydrationProfile(completion: completion)
    }
    
    public func resetPassword(email: String, appType: String, completion: @escaping (Result<String>) -> Void) {
        waterIOClient.resetPassword(email: email, appType: appType, completion: completion)
    }
    
    public func updateProfile(json: JSONDictionary, completion: @escaping (Result<String>) -> Void) {
        waterIOClient.updateProfile(json: json, completion: completion)
    }
    
    public func sendMessage(id: String, text: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.sendMessage(id: id, text: text, completion: completion)
    }
    
    public func updateMessage(id: String, status: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.updateMessage(id: id, status: status, completion: completion)
    }
    
    public func updateLogin(email: String, password: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.updateLogin(email: email, password: password, completion: completion)
    }
    
    public func getAllMissingMessage(completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.getUnrecivedMessages(completion: completion)
    }
    
    public func getVitaminsList(completion: @escaping (Result<[[String: Any]]>) -> Void) {
        waterIOClient.getVitaminsList(completion: completion)
    }
    
    public func getVitaminsList(completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.getUserScore(completion: completion)
    }
}

