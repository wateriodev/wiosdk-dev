import Foundation
import WaterIONetwork
import WaterIOBleKit
import UIKit

public class WIOApp {
    //Public
    public static let shared = WIOApp()

    public var processor: WIOCapDataProcessor
    public var wioBleManager: BleManagerType
    public var wioNetwork: WIONetwork!
    public var wioNotificationService: WIOPushNotificationManager!
    private var appConfig: AppConfigService?
    
    public var userId: Int? {
        didSet {
            if let uid = userId {
                wioNetwork.wioAnalytics.tracker.resume(userId: uid)
                wioNotificationService.userId = uid
            }
        }
    }
    
    public var appType: String!

    public var capUUID: String? {
       return CurrentDevice.currentDevice()?.UUID
    }
    
    private init() {
        userId = UserCredentialStore().userCredentials?.userId
        wioNotificationService = WIOPushNotificationManager()
        wioBleManager = BleManager()
        wioBleManager.setCapType(type: .base)
        processor = WIOCapDataProcessor()
        wioBleManager.capDataModel.delegate = processor
        wioBleManager.isAppInBackground = UIApplication.shared.applicationState == .background
    }
    
    public func start(appToken: String, appType: String) {
        self.appType = appType
        wioNetwork = WIONetwork(appType: appType, appToken: appToken, deviceId: capUUID)
        appConfig = AppConfigService(waterIOClient: wioNetwork.wioApi.waterIOClient)
        appConfig?.delegate = self
        appConfig?.start()
        wioNotificationService.userId = userId
        wioNetwork.wioApi.delegate = self
    }
}

extension WIOApp: AppConfigurationDelegate {
    
    public func onErrorRecived(_ error: [AppConfigError]) {
        print("Wio SDK can not configuration \(error.description)")
    }
    
    public func onAppConfigFinish() {
       print("WioSDK is Ready")
    }
}

extension WIOApp: WIOApiDelegate {
    func userIdUpdated(_ userId: Int) {
        self.userId = userId
    }
}
