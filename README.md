# WioSDK

[![CI Status](https://img.shields.io/travis/israel@water-io.com/WioSDK.svg?style=flat)](https://travis-ci.org/israel@water-io.com/WioSDK)
[![Version](https://img.shields.io/cocoapods/v/WioSDK.svg?style=flat)](https://cocoapods.org/pods/WioSDK)
[![License](https://img.shields.io/cocoapods/l/WioSDK.svg?style=flat)](https://cocoapods.org/pods/WioSDK)
[![Platform](https://img.shields.io/cocoapods/p/WioSDK.svg?style=flat)](https://cocoapods.org/pods/WioSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WioSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WioSDK'
```


##how to install
xocde 10.0
git clone git@bitbucket.org:wateriodev/wiosdk.git --branch swift-4.2

Xcode 10.2.1, 10.3
git clone git@bitbucket.org:wateriodev/wiosdk.git --branch swift-5

Xcode 11


## Author

israel@water-io.com, israel@water-io.com

## License

WioSDK is available under the MIT license. See the LICENSE file for more info.

## How cap work
1. connect to cap
2. cap enter to sleep mode
3. waiting for cap advertisement and awake again, get all data from cap and asleep again



##Requirements
Wio SDK
Swift 4.2
Xcode 10.2.1, 10.3 


Xcode 10.2 or later
Swift 5.0 or later


##Backwards compatibility
for Xcode 10 and Swift 4.2, use the 3.5.2 version



## When app Launching 

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    WIOApp.shared.start(appToken: "", appType: "")
      return true
    }
}
```


##  Cap Connetion
```swift

class CapConnectionViewController {

private lazy var capConnection: CapConnectionType = {
    let connection = CapConnection()
    connection.delegate = self
    return connection
}()

func start() {
    capConnection.startScan()
}

func stop() {
    capConnection.stopScan()
  }
}

extension CapConnectionViewController: CapConnectionDelegate {
    public func onDeviceConnectionStateChange(state: ConnectionState) {
    
    }
}
```


## Get cap events
```swift
 WIOApp.shared.processor.delegate = self
```

```swift

extension ViewController: CapDataProcessorDelegate {
    func capDataModelDidUpdateEvents(_ eventsList: [CapEvent], isCapOpen: Bool?) {

    }

    func didUpdateCapInfo(_ capVersion: String) {
    }
}
```

