//
//  WaterIOBleKit.h
//  WaterIOBleKit
//
//  Created by israel on 15/08/2019.
//  Copyright © 2019 Water-io. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WaterIOBleKit.
FOUNDATION_EXPORT double WaterIOBleKitVersionNumber;

//! Project version string for WaterIOBleKit.
FOUNDATION_EXPORT const unsigned char WaterIOBleKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WaterIOBleKit/PublicHeader.h>
#import "BootLoaderServiceModel.h"
#import "Constants.h"
#import "CyCBManager.h"
#import "NSData+hexString.h"
#import "NSString+hex.h"
#import "OTAFileParser.h"
#import "PocketCreator.h"
#import "Utilities.h"
#import "RZBPeripheral.h"
#import "RZBCentralManager.h"
#import "RZBScanInfo.h"


