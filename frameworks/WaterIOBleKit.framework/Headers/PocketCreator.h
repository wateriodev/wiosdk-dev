//
//  PocketCreator.h
//  WaterIOBleKit
//
//  Created by israel on 12/06/2019.
//

#import <Foundation/Foundation.h>


@interface PocketCreator : NSObject

+(NSData *)createPacketWithCommandCode:(uint8_t)commandCode
                            dataLength:(unsigned short)dataLength
                                  data:(NSDictionary *)dataDict checkSumType:(NSString *)checkSumType;

    
@end

